<?php

use Illuminate\Database\Seeder;

class BooksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('books')->insert(
            [
                [
                    'title' => 'GAME OF THRONES',
                    'author' => 'MARTIN, GEORGE',
                ],
                [
                    'title' => 'SHADOW OF NIGHT                    ',
                    'author' => 'HARKNESS, DEBORAH',
                ],
                [
                    'title' => 'PRIDE AND PREJUDICE                    ',              
                    'author' => 'Austen, J.',
                ],
                [
                    'title' => 'FIRE AND BLOOD',                 
                    'author' => 'MARTIN, GEORGE R. R',
                ],      [
                    'title' => 'Harry Potter 5',                  
                    'author' => 'J.K. Rowling',
                ],
            ]);
    }
}


<!DOCTYPE html>
<html>
    <head>
    </head>
    <body>
        <table>
            <tr>
                <th>ID</th>
                <th>title</th>
                <th>author</th>
            </tr>
            @foreach($books as $book)
            <tr>
                <td>{{$book->id}}</td>
                <td>{{$book->title}}</td>
                <td>{{$book->author}}</td>
            </tr>
            @endforeach
        </table>
    </body>
</html>